'use strict'
const Responses = require('./response');
const Dynamo = require('./Dynamo');
const TABLE_NAME = 'countries';
const COUNTRY_NAME_GCI = 'country_name_index'


const countryService = {

    async getAllCountries() {
        const countries = await Dynamo.getAllCountries(TABLE_NAME).catch(err => {
            console.log("error "+ err)
        });
        return Responses.success( countries )
    },
    
    async getCountryByName(countryName) {
        let name = this.formatCountry(countryName);
        const country = await Dynamo.getCountryByName(TABLE_NAME, COUNTRY_NAME_GCI, 'country_name', name ).catch(err => {
            console.log("error "+ err)
        });
        console.log(country)
        if(!country.id) 
            return Responses.failure("no country with that name");
        return Responses.success(country);
    },

    async saveCountry (countryName) {
        let name = this.formatCountry(countryName);
        const result = await Dynamo.saveCountry(name, TABLE_NAME).catch(err => {
            console.log("error "+ err)
        });
        return Responses.success( {message: "country is inserted successfully"} )    
    },

    formatCountry(countryName) {
        let name = countryName.toLowerCase();
        name =  name.charAt(0).toUpperCase() + name.slice(1);
        return name;
    }
}

module.exports = countryService