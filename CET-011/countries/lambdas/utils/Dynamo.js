const AWS = require('aws-sdk');

const documentClient = new AWS.DynamoDB.DocumentClient();

const Dynamo = {
    async getCountryById(id, TableName) {
        const params = {
            TableName,
            Key: {
                id,
            },
        };

        const data = await documentClient.get(params).promise();

        if (!data || !data.Item) {
            throw Error(`There was an error fetching the data for ID of ${id} from ${TableName}`);
        }
        console.log(data);

        return data.Item;
    },
    
    // async getCountryByName(name, TableName) {
    //     console.log(name)
    //     const params = {
    //       TableName : TableName,
    //     ExpressionAttributeValues: {
    //     ':country': 'egypt',
    //   },
    //      KeyConditionExpression: 'country_name = :country',
    //     }; 

    //     return await documentClient.query(params).promise();
        
    // },

    async getAllCountries(TableName) {
        const params = {
            TableName
        }; 

        return await documentClient.scan(params).promise(); 
    },
    
    async saveCountry(countryName, TableName) {
        const params = {
            TableName,
            Item: {id: new Date().getTime().toString() , country_name: countryName} 
        }; 
        
        return await documentClient.put(params).promise();
    },
    
    
    getCountryByName: async (tableName, index, queryKey, queryValue) => {
        const params = {
            TableName: tableName,
            IndexName: index,
            KeyConditionExpression: `${queryKey} = :hkey`,
            ExpressionAttributeValues: {
                ':hkey': queryValue,
            },
        };

        const res = await documentClient.query(params).promise();
        return res.Items[0] || {};
    },
    
    

};
module.exports = Dynamo;