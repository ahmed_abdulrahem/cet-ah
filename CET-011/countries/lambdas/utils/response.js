const Responses = {
    success(data = {}) {
        return {
            statusCode: 200,
            body: JSON.stringify(data),
        };
    },

    failure(msg) {
        return {
            statusCode: 400,
            body: msg,
        };
    },
};

module.exports = Responses;