'use strict';
const countryService = require('./utils/countryService');
const Responses = require('./utils/response');

module.exports.handler = async (event) => {
  
  // routing
  if(event.resource == '/countries' && event.httpMethod == 'GET')
    return countryService.getAllCountries();
  else if (event.resource == '/countries' && event.httpMethod == 'POST')
    return countryService.saveCountry(JSON.parse(event.body).country);
  else if (event.resource == '/countries/{name}')
    return countryService.getCountryByName(event.pathParameters.name);

};
