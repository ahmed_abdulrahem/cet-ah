# Serverless Application

## Main goal:
Create a country management application using serverless framework.
<br><br>

## Architecture:

<img title="Serverless diagram" src="serverless2.png">
<br><br>
 
## Components:
- Local machine.
- API gateway.
- Lambda function.
- DynamoDB.

<br><br>

## Description:
A serverless application containing an API gateway which intercepts the requests of the client and proxy them to the lambda which resolve the user request and handle it by dealing with the DynamoDB.
The Dynamodb is the persistance layer where we store and retrieve countries from it. 
The actions of the user are as follows: 
- Stroe new country.
- get All countries.
- get country by name.

<br><br>
## Implementation guide:
- Install AWS CLI.
- Install serverless framework.
- run serverless deploy.
- Use the API Gateway link to call the service.

## Commands
- serverless deploy
  